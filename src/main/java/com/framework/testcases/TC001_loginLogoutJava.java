package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_loginLogoutJava extends ProjectMethods {

	@BeforeTest
	public void setData()
	{
		testCaseName = "Create lead";
		testDescription = "Create a new Lead in leaftaps";
		testNodes="Leads";
		author = "Dhanesh";
		category = "Smoke";
		dataSheetName = "TestData";
	}
	
	@Test(dataProvider="fetchData")
	public void CreateLead(String UN, String PWD, String FirstName, String LastName, String CompanyName)
	{
		new LoginPage()
		.enterUsername(UN)
		.enterPassword(PWD)
		.clickLogin()
		.clickCRMSFA()	
		.clickLead()
		.clickCreateLead()
		.enterFirstname(FirstName)
		.enterLastname(LastName)
		.enterCompanyname(CompanyName)
		.clickCreateLeadButton()
		.verifyLead(CompanyName);
	}
	}
	


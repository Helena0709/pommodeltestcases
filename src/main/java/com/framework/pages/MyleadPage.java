package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyleadPage extends ProjectMethods{

	public MyleadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.XPATH,using="//a[text()='Create Lead']") WebElement eleCreateLead;
	@FindBy(how=How.XPATH,using="//a[text()='Create Lead']") WebElement eleFindlead;
	
	public CreateleadPage clickCreateLead() {
	    click(eleCreateLead);
	    return new CreateleadPage();
	}
	
	
	
}














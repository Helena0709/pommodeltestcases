package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateleadPage extends ProjectMethods{

	public CreateleadPage() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyname;
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFirstname;
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLastname;
	@FindBy(how = How.XPATH,using="//input[@class='smallSubmit']") WebElement eleClick;
	
	
	public CreateleadPage enterCompanyname(String data) {
		//WebElement eleUsername = locateElement("id", "username");
		clearAndType(eleCompanyname, data);
		return this; 
	}
	public CreateleadPage enterFirstname(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(eleFirstname, data);
		return this;
	}
	public CreateleadPage enterLastname(String data) {
		//WebElement elePassword = locateElement("id", "password");
		clearAndType(eleLastname, data);
		return this;
	}
	public ViewleadPage clickCreateLeadButton() {
		//WebElement eleLogin = locateElement("class", "decorativeSubmit");
	    click(eleClick);
	    /*HomePage hp = new HomePage();
	    return hp;*/ 
	    return new ViewleadPage();
	}
}













